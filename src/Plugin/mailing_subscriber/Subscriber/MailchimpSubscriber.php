<?php


namespace Drupal\mailing_subscriber_mailchimp\Plugin\mailing_subscriber\Subscriber;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mailing_subscriber\Annotation\Subscriber;
use Drupal\mailing_subscriber\Exception\MailingSubscriberException;
use Drupal\mailing_subscriber\Plugin\mailing_subscriber\Subscriber\SubscriberBase;
use Mailchimp;
use Mailchimp_List_AlreadySubscribed;

/**
 * Defines a mailchimp plugin.
 *
 * @Subscriber(
 *   id = "mailchimp",
 *   label = @Translation("Mailchimp"),
 *   admin_label = @Translation("Mailchimp"),
 *   category = @Translation("Subscriber"),
 * )
 */
final class MailchimpSubscriber extends SubscriberBase implements ContainerFactoryPluginInterface {

  /**
   * Get the label.
   *
   * @return string
   *   The label.
   */
  public function getLabel() {
    return t('Mailchimp type');
  }

  public function defaultConfiguration() {
    return [
        'api_key' => '',
        'list_id' => '',
        'double_opt_in' => FALSE,
        'send_welcome' => FALSE,
      ] + parent::defaultConfiguration();
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Mailchimp API key'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];
    $form['list_id'] = [
      '#type' => 'textfield',
      '#title' => t('Mailchimp mailing list ID'),
      '#default_value' => $this->configuration['list_id'],
      '#required' => TRUE,
    ];
    $form['double_opt_in'] = [
      '#type' => 'checkbox',
      '#title' => t('Activate double opt in'),
      '#default_value' => $this->configuration['double_opt_in'],
    ];
    $form['send_welcome'] = [
      '#type' => 'checkbox',
      '#title' => t('Send welcome email'),
      '#default_value' => $this->configuration['send_welcome'],
    ];

    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement validateForm() method.
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['api_key'] = $form_state->getValue('api_key');
    $this->configuration['list_id'] = $form_state->getValue('list_id');
    $this->configuration['double_opt_in'] = $form_state->getValue('double_opt_in');
    $this->configuration['send_welcome'] = $form_state->getValue('send_welcome');

  }

  /**
   * Subscribe the given email address.
   *
   * @param string $email
   *
   * @return bool
   * @throws \Drupal\mailing_subscriber\Exception\MailingSubscriberException
   */
  public function subscribe($email) {
    try {
      $client = $this->getClient();
      $double_opt_in = boolval($this->configuration['double_opt_in']);
      $send_welcome = boolval($this->configuration['send_welcome']);
      $list_id = $this->configuration['list_id'];
      $result = $client->lists->subscribe($list_id, ['email' => $email], NULL, 'html', $double_opt_in, false, true, $send_welcome);

      if (!empty($result)) {
        return TRUE;
      }
    }
    catch (Mailchimp_List_AlreadySubscribed $e) {
      return TRUE;
    }
    catch (\Mailchimp_Error $e) {
      $this->logger->error('@error_message', [
        '@error_message' => $e->getMessage(),
        'exception' => $e,
      ]);

      throw new MailingSubscriberException();
    }
    return FALSE;
  }

  /**
   * @return \Mailchimp
   * @throws \Mailchimp_Error
   */
  protected function getClient() {
    return new Mailchimp($this->configuration['api_key']);
  }

}
