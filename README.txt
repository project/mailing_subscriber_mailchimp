-- SUMMARY --
The mailing subscriber mailchimp module provides the mailchimp implementation of the mailing subscriber module.
This module provides a form to the configuration of the mailing subscriber block. The user can input the mailchimp API key
and the location ID. 
When an user clock on subscribe this module subscribes the given user email to the, in the configuration given, mailchimp list.

-- INSTALLATION -- 
 - Install as usual, see http://drupal.org/node/1897420 for further information.

-- REQUIREMENTS --
 - mailing_subscriber - http://drupal.org/project/mailing_subscriber

-- AVAILABLE IMPLEMENTATIONS -- 
 - Mailchimp - http://drupal.org/project/mailing_subscriber_mailchimp

-- USAGE -- 
 - Install the module as described above, this module will automatically install the mailing subscriber module.
 - Create a new block
 - Configure this block following the stept
 - Save the block
 - An email field with a subscribe button is shown on the block
 - Insert a valid email and click on subscribe
 - You are now subscribed to a mailing list